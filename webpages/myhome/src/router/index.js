import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/index'
import Test from '@/components/Test'
import DashBoard from '@/components/dashboard'
import iView from 'iview';
import 'iview/dist/styles/iview.css'; // 使用 CSS
Vue.use(Router)
Vue.use(iView)
export default new Router({
	routes: [{
		path: '/',
		component: Index,
		children: [{
			path: '/dashboard',
			component: resolve => require(['../components/dashboard.vue'], resolve)
		},
		{
			path: '/serverstatus',
			component: resolve => require(['../components/ServerStatus.vue'], resolve)
		}]
	}, {
		path: '/hello',
		component: resolve => require(['../components/HelloWorld.vue'], resolve)
	}, {
		path: '/test',
		component: resolve => require(['../components/Test.vue'], resolve)
	}]
})