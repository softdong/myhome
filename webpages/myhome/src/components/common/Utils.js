export default {

	getElementSize(id) {
		let dom = document.getElementById(id);
		return {
			width: dom.clientWidth,
			height: dom.clientHeight
		}

	}

}