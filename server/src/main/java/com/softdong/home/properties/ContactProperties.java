package com.softdong.home.properties;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

public class ContactProperties implements Serializable{
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String url;
    @Getter
    @Setter
    private String email;
}
